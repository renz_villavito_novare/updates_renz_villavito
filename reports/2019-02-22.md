# 2019-02-22

## OneGie (Globe)

**Updates Today**

- daily standup
- bug fixing
- QA support

**Callouts**

- no issues

**Up Next**

- daily standup
- DFS: document for globe
- bug fixing
- QA support